#ifndef _SHAPES_H_
#define _SHAPES_H_

#include <string>
#include <math.h>

class Shape {
  private:
    std::string name;
  public:
    Shape(std::string name);
    virtual ~Shape();
    std::string description(){
      return name + " has area: " + std::to_string(area());
    }
    virtual double area() = 0;
};

class Square : public Shape {
  private:
    double width;
  public:
    Square(double width);
    double area();
};

class Rectangle : public Shape {
  private:
    double width;
    double height;
  public:
    Rectangle(double width, double height): Shape("Rectangle"){
      this->width = width;
      this->height = height;
    }
    double area(){
      return height*width;
    }
};

class Circle : public Shape {
  private:
    double radius;
  public:
    Circle(double radius): Shape("Circle"){
      this->radius = radius;
    }
    double area(){
      return radius * radius * M_PI;
    }
};

#endif
