#include "iostream"
#include "shapes.h"
#include <vector>

Shape::Shape(std::string name){
  this->name = name;
}

Shape::~Shape(){
}

// std::string Shape::description(){
//   return name + " has area: " + std::to_string(area());
// }

Square::Square(double width) : Shape("Square"){
  this->width = width;
}

double Square::area(){
  return width * width;
}

int main(){
  std::vector <Shape*> myshapes;
  myshapes.push_back(new Circle(10.2));
  myshapes.push_back(new Rectangle(14.6, 8.7));
  myshapes.push_back(new Square(5.9));

  for (unsigned i = 0; i < myshapes.size(); ++i)  {
    std::cout << myshapes[i]->description() << std::endl;
  }
  
  for (Shape * s : myshapes) {
    delete s;
  }
  myshapes.clear();


  // Square shape1(4); 
  // std::cout << "--------------------------- \n" << shape1.description() << std::endl;
  
  // Rectangle shape2(4,8); 
  // std::cout << "--------------------------- \n" << shape2.description() << std::endl;
}
